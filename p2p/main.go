package main

import (
	"bufio"
	"context"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"flag"
	"fmt"
	"log"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	golog "github.com/ipfs/go-log"
	libp2p "github.com/libp2p/go-libp2p"
	crypto "github.com/libp2p/go-libp2p-crypto"
	host "github.com/libp2p/go-libp2p-host"
	net "github.com/libp2p/go-libp2p-net"
	peerLib "github.com/libp2p/go-libp2p-peer"
	pstore "github.com/libp2p/go-libp2p-peerstore"
	ma "github.com/multiformats/go-multiaddr"
	gologging "github.com/whyrusleeping/go-logging"
)

type Block struct {
	Index     int
	Timestamp string
	Pacient   string
	Medicine	string
	Quantity 	int
	Hash      string
	PrevHash  string
}

type Message struct {
	Pacient string
	Medicine string
	Quantity int
}

type MessageEdit struct {
	Index int
	Pacient string
	Medicine string
	Quantity int
}

var Blockchain []Block
var mutex = &sync.Mutex{}

var globalIp string
var globalPort int
var globalIpfs string

func makePeerHost(ip string, listenPort int) (host.Host, error) {
	var r io.Reader
	r = rand.Reader

	priv, _, err := crypto.GenerateKeyPairWithReader(crypto.RSA, 2048, r)
	if err != nil {
		return nil, err
	}

	opts := []libp2p.Option{
		libp2p.ListenAddrStrings(fmt.Sprintf("/ip4/"+ip+"/tcp/%d", listenPort)),
		libp2p.Identity(priv),
	}

	basicHost, err := libp2p.New(context.Background(), opts...)
	if err != nil {
		return nil, err
	}

	hostAddr, _ := ma.NewMultiaddr(fmt.Sprintf("/ipfs/%s", basicHost.ID().Pretty()))
	addrs := basicHost.Addrs()

	var addr ma.Multiaddr
	for _, i := range addrs {
		if strings.HasPrefix(i.String(), "/ip4") {
			addr = i
			break
		}
	}
	globalIpfs = hostAddr.String()
	fullAddr := addr.Encapsulate(hostAddr)
	log.Printf("Peer: %s\n", fullAddr)
	log.Printf("Web API: http://%s:%d\n", ip, listenPort + 1)

	return basicHost, nil
}

func makeMuxRouter() http.Handler {
	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/", handleGetBlockchain).Methods("GET")
	muxRouter.HandleFunc("/valid", handleGetValid).Methods("GET")
	muxRouter.HandleFunc("/", handleWriteBlock).Methods("POST")
	muxRouter.HandleFunc("/edit", handleEditBlock).Methods("POST")
	muxRouter.HandleFunc("/info", handleGetPeerInfo).Methods("GET")
	return muxRouter
}

func respondWithJSON(w http.ResponseWriter, r *http.Request, code int, payload interface{}) {
	response, err := json.MarshalIndent(payload, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("HTTP 500: Internal Server Error"))
		return
	}
	w.WriteHeader(code)
	w.Write(response)
}

func handleGetPeerInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	type Entry struct {
		IP string `json:"ip"`
		PORTS int `json:"port"`
		IPFS string `json:"ipfs"`
	}

	e := Entry{IP: globalIp, PORTS: globalPort, IPFS: globalIpfs}
	bytes, err := json.Marshal(e)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.WriteString(w, string(bytes))
}

func handleGetBlockchain(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	bytes, err := json.MarshalIndent(Blockchain, "", "  ")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	io.WriteString(w, string(bytes))
}

func handleGetValid(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")

	type Entry struct {
		Valid bool `json:"valid"`
	}
	e := Entry{ Valid: isBlockchainValid(Blockchain) }
	bytes, _ := json.Marshal(e)
	io.WriteString(w, string(bytes))
}

func handleWriteBlock(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	temp, _ := ioutil.ReadAll(r.Body)

	var m Message
	err := json.Unmarshal(temp, &m)
	if  err != nil {
		respondWithJSON(w, r, http.StatusBadRequest, r.Body)
		return
	}
	defer r.Body.Close()

	newBlock := generateBlock(Blockchain[len(Blockchain)-1], m.Pacient, m.Medicine, m.Quantity)

	if isBlockValid(newBlock, Blockchain[len(Blockchain)-1]) {
		mutex.Lock()
		Blockchain = append(Blockchain, newBlock)
		mutex.Unlock()
	}
	respondWithJSON(w, r, http.StatusCreated, newBlock)
}

func handleEditBlock(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	temp, _ := ioutil.ReadAll(r.Body)

	var m MessageEdit
	err := json.Unmarshal(temp, &m)
	if  err != nil {
		respondWithJSON(w, r, http.StatusBadRequest, r.Body)
		return
	}
	defer r.Body.Close()

	mutex.Lock()
	Blockchain[m.Index].Pacient = m.Pacient
	Blockchain[m.Index].Medicine = m.Medicine
	Blockchain[m.Index].Quantity = m.Quantity
	mutex.Unlock()
	respondWithJSON(w, r, http.StatusCreated, Blockchain[m.Index])
}
func runWebAPI(ip string, port int) {
	mux := makeMuxRouter()
	s := &http.Server{
		Addr:           ip+":"+ strconv.Itoa(port),
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	if err := s.ListenAndServe(); err != nil {
		log.Fatalln(err)
	}
}

func handleStream(s net.Stream) {
	log.Println("Got a new stream!")
	rw := bufio.NewReadWriter(bufio.NewReader(s), bufio.NewWriter(s))

	go readData(rw)
	go writeData(rw)
}

func readData(rw *bufio.ReadWriter) {
	for {
		str, err := rw.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		if str == "" {
			return
		}
		if str != "\n" {
			chain := make([]Block, 0)
			if err := json.Unmarshal([]byte(str), &chain); err != nil {
				log.Fatal(err)
			}

			mutex.Lock()
			if len(chain) > len(Blockchain) &&  isBlockchainValid(chain) {
				Blockchain = chain
				bytes, err := json.MarshalIndent(Blockchain, "", "  ")
				if err != nil {
					log.Fatal(err)
				}
				fmt.Printf("%s", string(bytes))
			}
			mutex.Unlock()
		}
	}
}

func writeData(rw *bufio.ReadWriter) {
	go func() {
		for {
			time.Sleep(5 * time.Second)
			mutex.Lock()
			bytes, err := json.Marshal(Blockchain)
			if err != nil {
				log.Println(err)
			}
			mutex.Unlock()

			mutex.Lock()
			rw.WriteString(fmt.Sprintf("%s\n", string(bytes)))
			rw.Flush()
			mutex.Unlock()

		}
	}()

	stdReader := bufio.NewReader(os.Stdin)
	
	for {
		fmt.Print("> ")
		sendData, err := stdReader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		sendData = strings.Replace(sendData, "\n", "", -1)
		sendData = strings.Replace(sendData, "\r", "", -1)
		newBlock := generateBlock(Blockchain[len(Blockchain)-1], "Hola", "Como", 456)

		if isBlockValid(newBlock, Blockchain[len(Blockchain)-1]) {
			mutex.Lock()
			Blockchain = append(Blockchain, newBlock)
			mutex.Unlock()
		}

		spew.Dump(Blockchain)
	}
}

func main() {
	t := time.Now().Format(time.RFC3339)
	genesisBlock := Block{}
	genesisBlock = Block{0, t, "", "", 0, calculateHash(genesisBlock), ""}

	Blockchain = append(Blockchain, genesisBlock)

	golog.SetAllLoggers(gologging.INFO)

	ip := flag.String("ip", "127.0.0.1", "wait for incoming connections")
	port := flag.Int("port", 8000, "port")
	peer := flag.String("peer", "", "peer to dial")
	flag.Parse()

	globalIp = *ip
	globalPort = *port

	ha, err := makePeerHost(*ip, *port)

	if err != nil {
		log.Fatal(err)
	}

	go runWebAPI(*ip, *port + 1)

	if *peer == "" {
		ha.SetStreamHandler("/p2p/1.0.0", handleStream)
		select {}
	} else {
		ha.SetStreamHandler("/p2p/1.0.0", handleStream)
		ipfsaddr, err := ma.NewMultiaddr(*peer)
		if err != nil {
			log.Fatalln(err)
		}

		pid, err := ipfsaddr.ValueForProtocol(ma.P_IPFS)
		if err != nil {
			log.Fatalln(err)
		}

		peerid, err := peerLib.IDB58Decode(pid)
		if err != nil {
			log.Fatalln(err)
		}

		targetPeerAddr, _ := ma.NewMultiaddr(
			fmt.Sprintf("/ipfs/%s", peerLib.IDB58Encode(peerid)))
		targetAddr := ipfsaddr.Decapsulate(targetPeerAddr)
		ha.Peerstore().AddAddr(peerid, targetAddr, pstore.PermanentAddrTTL)
		s, err := ha.NewStream(context.Background(), peerid, "/p2p/1.0.0")
		if err != nil {
			log.Fatalln(err)
		}
		rw := bufio.NewReadWriter(bufio.NewReader(s), bufio.NewWriter(s))

		go writeData(rw)
		go readData(rw)

		select {}
	}
}

func isBlockchainValid (blockchain []Block) bool {
	for index := 1; index < len(blockchain); index++ {
		isValid := isBlockValid(blockchain[index], blockchain[index-1])
		if !isValid {
			return false
		}
	}
	return true
}

func isBlockValid(newBlock, oldBlock Block) bool {
	if oldBlock.Index+1 != newBlock.Index {
		return false
	}

	if oldBlock.Hash != newBlock.PrevHash {
		return false
	}

	if calculateHash(newBlock) != newBlock.Hash {
		return false
	}
	return true
}

func calculateHash(block Block) string {
	record := strconv.Itoa(block.Index) + block.Timestamp + block.Pacient + block.Medicine + strconv.Itoa(block.Quantity) + block.PrevHash
	h := sha256.New()
	h.Write([]byte(record))
	hashed := h.Sum(nil)
	return hex.EncodeToString(hashed)
}

func generateBlock(oldBlock Block, Pacient string, Medicine string, Quantity int) Block {
	var newBlock Block

	t := time.Now().Format(time.RFC3339)

	newBlock.Index = oldBlock.Index + 1
	newBlock.Timestamp = t
	newBlock.Pacient = Pacient
	newBlock.Medicine = Medicine
	newBlock.Quantity = Quantity
	newBlock.PrevHash = oldBlock.Hash
	newBlock.Hash = calculateHash(newBlock)
	return newBlock
}